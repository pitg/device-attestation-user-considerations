# User considerations for device attestation

### Status of this document

Outline of user considerations for device attestation, which may be helpful in evaluating proposals at IETF and W3C for anti-fraud work. This expands on [earlier, quick comments on device integrity attestation](https://github.com/antifraudcg/proposals/issues/8#issuecomment-1158928350).

Editor: [Nick Doty](https://npdoty.name), <ndoty@cdt.org>

## Abstract

Attesting to properties of devices or users is an area of interest for anti-fraud protection, as it allows for reputational or other signals from one trusted entity to be employed by other services, in protecting either the service or other users from some kinds of abuse.

However, attestation also introduces additional concerns for users in how the attestation technology itself may be abused, against their individual or collective interests. This document provides questions that may identify issues in several categories where users may be adversely affected. In order to be respectful of human rights, these concerns should be addressed by any device attestation mechanism.

### Definitions

user

relying site (or origin)

attester

## Rigidity

Will the user be denied access, slowed access, be put through some burdensome process or otherwise punished because they can't or don't want to satisfy the attestation process? If the availability of some attestation mechanism reaches a certain percentage of the market, is it likely that sites will rely on it in a more rigid way? Is there an effective way to mitigate that reliance?

User control may be an ineffective protection if attestation mechanisms are widely available and heavily relied upon.

Consider how the system might be abused to discriminate against users with older devices, devices or software that has a smaller market share, or users from a particular geography.

Discrimination concerns are not hypothetical, but already documented in existing anti-fraud and other scenarios.

Greasing and holdbacks or holdouts have been proposed to mitigate rigidity in some contexts, including attestation. The efficacy of those mitigations is not yet clear.

## Granularity

What specifically is being attested, and at what level of detail? Is the attestation "looks-good" vs. "looks-bad", just "looks human", or will it be more detailed ("this device/user satisfies conditions X, Y and Z but not W")? More metadata would increase privacy risk, but likely also freedom risk.

Consider how the system might be abused to force both disclosure of additional information and compliance with additional restrictions on behavior. Can the granularity of the attestation change after deployment?

Once some attestation is available, there may be a tendency to attesting more details about the user, their behavior or their device. Discrimination, privacy and freedom are all at risk once relying sites can more easily insist on additional granularity of attestation.

## Freedom

Can users who control their own software and their own device still be attested? Can a user still install extensions, configure and customize their browser? Use open source software? Are DRM-like capabilities needed for the attestation to be acceptable to relying sites?

## Consolidation

Will attestation depend on their being a small number of attesters? Will attestation contribute to further consolidation of platform or browser providers?

## Privacy

What information needs to be shared with an attester to prove goodness, and what is done with that data? 

What is revealed to a relying site, from the availability of an attestation, who the attester was, or what metadata is available in the attestation? Highly granular attestations are of particular concern here, but even low granularity determinations may themselves be sensitive, depending on the semantics of the attestation.

Because attestation is typically intended to be cross-origin and stable, it may also be abused to identify users and their browsing activity. Signals may be abused for this purpose even if they aren't alone detailed enough for identifying the user, because they can be combined with other properties (IP address, browser configuration, etc.). [BROWSER-FINGERPRINTING]

Can attestations be linked to navigations to a relying site? What is revealed if attesters and origins collude against the user? What protections are provided against collusion?

_There are more detailed privacy considerations, including different types of linkability; see, for example, the Privacy Pass Architecture: https://datatracker.ietf.org/doc/draft-ietf-privacypass-architecture/_

## Efficacy

Efficacy may primarily be a consideration for relying sites, but some evaluation of efficacy against a particular threat model is also relevant as a user consideration because mechanisms can be designed that are ineffective against attackers but still costly to users.

Is the attestation reasonably effective? Against what threat model? 

Some proposals are designed to ask the browser itself to provide a promised attestation on behalf of the user; these are both more trivially defeated by the attacker and simultaneously burdensome to users. 

(DRM proposals in the past have taken the form where an organized attacker can circument them, but the burden is instead placed on users and the developers of consumer software and hardware.)

## Alternatives

Current mechanisms for fraud prevention often rely on invasive or pervasive data collection, including browser fingerprinting or identity verification. Attestation may be proposed as an alternative, where a signal of trust that isn't identifying or revealing is shared instead.

Will this replace more privacy-invasive alternatives? Or will it just be an additional mechanism, where data is combined with existing data collection? 

What assurance does the user community have that this will be a substitution of more invasive methods vs an additional mechanism?

---

## Acknowledgements

Thanks for input:

* pitg
* W3C Antifraud CG
* ciphertext